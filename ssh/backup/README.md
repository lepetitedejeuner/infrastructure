# Backup server
This is just a Linux machine that other machines can SCP files to so there is a
backup.  It also serves as an example of reporting server health metrics to a
database so they can be monitored by a Network Operations Cetner (NOC).  This
VM will send statistics to an [influxdb](../../www/influxdb) server, and those
metrics can be viewed by the [grafana](../../www/grafana) server.

# Customization
To try this one out for your domain, you'll want to:
- [Do the normal things](../../README.md#Customization)

# Testing
Scripts tested with:
- Ubuntu 18.04
