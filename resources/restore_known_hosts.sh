#!/bin/sh -e
# Restore known_hosts from a backup
# Works on any server
# Usually invoked by restore*.sh at deployment time
#### PREREQUISITES
# - Backed up known_hosts file is in ~root or /vagrant
#     - TIMESTAMP-root-known_hosts
# - Timestamp may be specified as the only argument
# - If $HOME/backup_password exists, it will be used to decrypt the backups

cd $HOME
TIMESTAMP=""
if [ ! -z "$1" ]; then
    TIMESTAMP="$1"
fi
if [ -z "$TIMESTAMP" ]; then
    TIMESTAMP=`ls -tr . | grep -- -root-known_hosts | tail -n 1 | sed 's/-.*//g'`
    echo "No timestamp given.  Using $TIMESTAMP"
fi

if [ -f "$HOME/backup_password" ]; then
        echo "Password found, backups will be decrypted before being restored"
        DECRYPT_CMD="openssl aes-256-cbc -d -pbkdf2 -pass file:$HOME/backup_password"
else
        echo "No password file found, encryption-at-rest is disabled"
        DECRYPT_CMD="cat"
fi

if [ -f "$TIMESTAMP-root-known_hosts" ]; then
    mkdir -p /root/.ssh
    cat "$TIMESTAMP-root-known_hosts" | $DECRYPT_CMD | cat > /root/.ssh/known_hosts
else
    echo "Unable to restore known_hosts file, missing: $TIMESTAMP-root-known_hosts"
fi
