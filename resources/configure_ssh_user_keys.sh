#!/bin/bash
# Add public keys for the named users to .ssh/authorized_keys and
#  create a new key for root
# Works on any server
# Usually invoked by vagrant up
#### PREREQUISITES
# - These files exist and contain public keys for the corresponding
#   USERs:
#   - $HOME/resources/USER.pub
# - Expects to be run as root

cd $HOME/resources
if [ $# -lt 1 ]; then
	echo "Usage: $0 USER ... [REMOTEUSER@REMOTESERVER]"
	exit 1
fi

REMOTEUSERS=()

# ssh-keygen will ensure that ~/.ssh exists
ssh-keygen -t ed25519 -N "" -f ~/.ssh/id_ed25519

for USER in "$@"
do
    PUBKEYFILE=./$USER.pub
    if [ -f $PUBKEYFILE ]; then
	    echo "Adding ssh access for $USER"
        cat "$PUBKEYFILE" >> $HOME/.ssh/authorized_keys
    else
        REMOTEUSERS+=("    - $USER")
        # If we know the fingerprints for these hosts, add them to known_hosts
        if [ -f "${USER##*@}.known_hosts" ]; then
            cat "${USER##*@}.known_hosts" >> ~root/.ssh/known_hosts
        fi
    fi
done
      
if [ "${#REMOTEUSERS[@]}" -gt 0 ]; then
    cat ~root/.ssh/id_ed25519.pub
    echo "Copy the above key to:"
    for i in `seq 0 ${#REMOTEUSERS[@]}`; do echo ${REMOTEUSERS[i]}; done
fi
