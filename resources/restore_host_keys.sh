#!/bin/bash
# Restore host keys from a backup
# Works on any server
# Usually invoked by restore*.sh at deployment time
#### PREREQUISITES
# - Backed up host keys (6 files in total) are in $HOME
#     - TIMESTAMP-ssh_host{ecdsa,ed25519,rsa}_key[.pub]
# - Timestamp may be specified as the only argument
# - If $HOME/backup_password exists, it will be used to decrypt the backups

cd $HOME
BACKUPTIMESTAMP=""
if [[ "$1" != "" ]]; then
    BACKUPTIMESTAMP="$1"
fi
if [[ "$BACKUPTIMESTAMP" == "" ]]; then
    BACKUPTIMESTAMP=`ls -tr . | grep -- -ssh_host_ | tail -n 1 | sed 's/-.*//g'`
    echo "No timestamp given.  Using $BACKUPTIMESTAMP"
fi

if [ -f "$HOME/backup_password" ]; then
        echo "Password found, backups will be decrypted before being restored"
        DECRYPT_CMD="openssl aes-256-cbc -d -pbkdf2 -pass file:$HOME/backup_password"
else
        echo "No password file found, encryption-at-rest is disabled"
        DECRYPT_CMD="cat"
fi

function move_keypair {
    # Restores public and private key for the given timestamp and algorithm
    TIMESTAMP="$1"
    ALGORITHM="$2"
    if [[ -f "${TIMESTAMP}-ssh_host_${ALGORITHM}_key" &&
          -f "${TIMESTAMP}-ssh_host_${ALGORITHM}_key.pub" ]]; then
        cat "${TIMESTAMP}-ssh_host_${ALGORITHM}_key" | $DECRYPT_CMD | cat > /etc/ssh/ssh_host_${ALGORITHM}_key
        cat "${TIMESTAMP}-ssh_host_${ALGORITHM}_key.pub" | $DECRYPT_CMD | cat > /etc/ssh/ssh_host_${ALGORITHM}_key.pub
    else
        echo "Unable to find ${ALGORITHM} keypair: ${TIMESTAMP}-ssh_host_${ALGORITHM}_key[.pub]"
    fi
}

move_keypair "$BACKUPTIMESTAMP" ecdsa
move_keypair "$BACKUPTIMESTAMP" ed25519
move_keypair "$BACKUPTIMESTAMP" rsa

# reload the new keys
systemctl restart ssh
