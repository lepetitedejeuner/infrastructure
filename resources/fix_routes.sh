#!/bin/sh -e
#
# This will fix the routes on vagrant boxes so the default route
# goes through the bridged network interface instead of the NAT
# interface.  It also properly sets the nameservers to use the
# local ones.
#
# Usage: $0 [-g GATEWAY] [-d ADDITIONAL_DNS]
#
#   GATEWAY - The IP of the default gateway (default: 192.168.1.1)
#   ADDITIONAL_DNS - Additional DNS servers to use (default: 192.168.1.100/200)
#

if [ `whoami` != "root" ]; then
	echo "$0 must be run as root."
	echo
	exit 1
fi

# Defaults
gateway="192.168.1.1"
dns="192.168.1.100,192.168.1.200"

# Get CLI options
while getopts g:d: f
do
    case $f in
        g)      gateway=$OPTARG;;
        d)      dns=$OPTARG;;
        \?)     echo $USAGE; exit 1;;
    esac
done
shift `expr $OPTIND - 1`

if [ -f /etc/netplan/50-vagrant.yaml ]; then
	echo "      routes:" >> /etc/netplan/50-vagrant.yaml
	echo "      - to: 0.0.0.0/0" >> /etc/netplan/50-vagrant.yaml
	echo "        via: $gateway" >> /etc/netplan/50-vagrant.yaml
	echo "      nameservers:" >> /etc/netplan/50-vagrant.yaml
	echo "        addresses: [$dns]" >> /etc/netplan/50-vagrant.yaml
	netplan generate
	netplan apply

	sleep 2 # apparently it takes a couple seconds to apply sometimes?
fi

doas=`ls /etc/doas* 2> /dev/null | true`
if [ ! -z "$doas" ]; then
	# We need to do this in OpenBSD syntax
	route del default
	route add default $gateway
else
	echo "Looking for route"
	route=`which route | true`
	if [ ! -z "$route" ]; then
		# We should use the Linux (normal?) route syntax
		# There can be more than one default route (e.g. matches 0.0.0.0)
		# so we want to make sure we specify deleting the correct one
		route del default gw 10.0.2.2
	else
		ip route del 10.0.2.2
	fi
fi

# Lastly, we disable DNSSEC because it causes problems sometimes (Ubuntu 18.04)
sed -ie 's/DNSSEC=.*/DNSSEC=no/g' /etc/systemd/resolved.conf
systemctl restart systemd-resolved
