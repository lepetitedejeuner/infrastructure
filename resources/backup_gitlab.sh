#!/bin/sh -e
# Back up gitlab to a remote machine
# Works on any gitlab server
# install_backups.sh usually installs this script in /root/resources
# Usually invoked by /root/backup.sh
#### PREREQUISITES
# - All vagrant provisioning is complete, including:
#   - rsync
#   - tar (1.3.0 or later)
# - Root's ssh key is in DESTINATION_HOST's .ssh/authorized_keys
# - If $HOME/backup_password exists, backups will be encrypted
# - Must be run as root

DESTINATION_HOST=""
DESTINATION_PATH=""

if [ $# -eq 1 -o $# -gt 2 ]; then
	echo "Usage: $0 [DESTINATION_HOST DESTINATION_PATH]"
	exit 1
elif [ $# -eq 2 ]; then
	DESTINATION_HOST="$1"
	DESTINATION_PATH="$2"
fi

BACKUP_PATH=`grep "'backup_path'" /etc/gitlab/gitlab.rb | sed -e 's/.*= "//g' -e 's/".*//g'`
echo "Backups will be stored according to the backup_path: $BACKUP_PATH"
if [ ! -z "$DESTINATION_HOST" -a ! -z "$DESTINATION_PATH" ]; then
	echo "Backups will be copied to $DESTINATION_HOST:$DESTINATION_PATH"
fi

if [ -f "$HOME/backup_password" ]; then
	echo "Password found, encrypting backups before sending them off-site"
	ENCRYPT_CMD="openssl enc -aes-256-cbc -salt -pbkdf2 -pass file:$HOME/backup_password"
else
	echo "No password file found, encryption-at-rest is disabled"
	ENCRYPT_CMD="cat"
fi

# gitlab-rake will backup everything except the config file and TLS certs
gitlab-rake gitlab:backup:create STRATEGY=copy  # takes a few minutes
# STRATEGY=copy will avoid corrupt backups due to data changing during the backup process

LATEST_FILE=`ls -tr $BACKUP_PATH | tail -n 1`
TIMESTAMP=`echo $LATEST_FILE | sed -e 's/_.*//g'`
if [ ! -z "$DESTINATION_HOST" -a ! -z "$DESTINATION_PATH" ]; then
	cat "${BACKUP_PATH}/$LATEST_FILE" | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$LATEST_FILE"
	# If we copied it off site, delete it so we don't run out of disk space
	echo "Copied $LATEST_FILE to backup machine, now deleting"
	rm ${BACKUP_PATH}/$LATEST_FILE

	# Now lets make a backup of the config file. In theory, the config file is
	# already in git, so we shouldn't NEED to do this, but it's easy enough and
	# it's better to be safe than sorry (e.g. if someone updated the live config
	# file, or forgot to commit/push to the git repo).
	cat /etc/gitlab/gitlab.rb | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-gitlab.rb"
	cat /etc/gitlab/gitlab-secrets.json | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-gitlab-secrets.json"

	# We don't want our host keys to change when we re-deploy
	/root/resources/backup_host_keys.sh "$TIMESTAMP" "$DESTINATION_HOST" "$DESTINATION_PATH"

	# We don't want our host keys to change when we re-deploy
	/root/resources/backup_known_hosts.sh "$TIMESTAMP" "$DESTINATION_HOST" "$DESTINATION_PATH"

        # Back up root's keypair
        /root/resources/backup_ssh_keys.sh "$TIMESTAMP" "$DESTINATION_HOST" "$DESTINATION_PATH"

	# Finally we backup our TLS certs, if they exist
	if [ -d /etc/gitlab/ssl ]; then
		cd /etc/gitlab/ssl
		tar -Jc ./* | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-ssl.tar.xz"
	fi
fi
