#!/bin/sh -e
# Restore files from a gitlab backup
# Works on any gitlab server
# Usually invoked by vagrant up
#### PREREQUISITES
# - Must be run as root
# - Backup files from backup_gitlab.sh are in $HOME
#     - TIMESTAMP_DATE_VERSION-ee_gitlab_backup.tar
#     - TIMESTAMP-gitlab-secrets.json
#     - TIMESTAMP-ssl/*
#     - TIMESTAMP-ssh_host_*
# - VERSION of backup files exactly matches version of gitlab
# - resources directory is in $HOME
# - If $HOME/backup_password exists, it will be used to decrypt the backups
# - If arguments are given, all are required:
#   TIMESTAMP, BACKUPDATE, GITLAB_VERSION
#   Note: this script only supports the enterprise version, so the -ee is
#         omitted from the GITLAB_VERSION

if [ ! -z "$1" ]; then
	BACKUPTIMESTAMP="$1"
fi
if [ ! -z "$2" ]; then
	BACKUPDATE="$2"
fi
if [ ! -z "$3" ]; then
	BACKUPVERSION="$3"
fi

cd $HOME
if [ -z "$1" -o -z "$2" -o -z "$3" ]; then
	echo "TIMESTAMP, DATE, and VERSION arguments not given... autodetecting"
	PREFIX=`ls *-ee_gitlab_backup.tar | tail -n 1 | sed -e 's/_gitlab_backup.tar//g'`

	if [ -z "$PREFIX" ]; then
		echo "No TIMESTAMP_DATE_VERSION-ee_gitlab_backup.tar in ."
		exit 1
	fi

	BACKUPTIMESTAMP=`echo $PREFIX | sed -e 's/_.*//g'`
	BACKUPDATE=`echo $PREFIX | sed -e 's/^[0-9]*_\(.*\)_.*$/\1/g'`
	BACKUPVERSION=`echo $PREFIX | sed -e 's/^.*\_\(.*\)$/\1/g'`
fi
PREFIX="${BACKUPTIMESTAMP}_${BACKUPDATE}_${BACKUPVERSION}-ee"

INSTALLEDVERSION=`dpkg-query -W gitlab-ee | sed -e 's/.*\t\(.*-[^.]*\).*/\1/'`
if [ "$BACKUPVERSION-ee" != "$INSTALLEDVERSION" ]; then
	echo "Backup version $BACKUPVERSION-ee does not match $INSTALLEDVERSION"
	exit 1
fi


BACKUPSECRETS=${BACKUPTIMESTAMP}-gitlab-secrets.json
if [ ! -f "$BACKUPSECRETS" ]; then
	echo "No $BACKUPSECRETS in ."
	exit 1
fi
CONFIG=${BACKUPTIMESTAMP}-gitlab.rb
if [ ! -f "$CONFIG" ]; then
	echo "No $CONFIG in ."
	exit 1
fi

# We aren't checking for this because restoring it is optional
BACKUPTLSDIR=${BACKUPTIMESTAMP}-ssl.tar.xz

echo "Trying to restore $BACKUPTIMESTAMP from $BACKUPDATE"
if [ -f "$HOME/backup_password" ]; then
        echo "Password found, backups will be decrypted before being restored"
        DECRYPT_CMD="openssl aes-256-cbc -d -pbkdf2 -pass file:$HOME/backup_password"
else
        echo "No password file found, encryption-at-rest is disabled"
        DECRYPT_CMD="cat"
fi

cat ${PREFIX}_gitlab_backup.tar | $DECRYPT_CMD | cat > /var/opt/gitlab/backups/${PREFIX}_gitlab_backup.tar
chgrp git /var/opt/gitlab/backups/${PREFIX}_gitlab_backup.tar
chmod 660 /var/opt/gitlab/backups/${PREFIX}_gitlab_backup.tar

# Stop the processes that are connected to the database. Leave the rest of GitLab running:
gitlab-ctl stop unicorn
gitlab-ctl stop sidekiq

# Verify
#gitlab-ctl status

# Next, restore the backup, specifying the timestamp of the backup you wish to restore:
# This command will overwrite the contents of your GitLab database!
yes "yes" | gitlab-rake gitlab:backup:restore BACKUP=${PREFIX}

# Next, restore /etc/gitlab/gitlab-secrets.json
cat "$BACKUPSECRETS" | $DECRYPT_CMD | cat > /etc/gitlab/gitlab-secrets.json
# Restore the config file
cat "$CONFIG" | $DECRYPT_CMD | cat > /etc/gitlab/gitlab.rb

# Restore our host keys
./resources/restore_host_keys.sh "$BACKUPTIMESTAMP"

# Restore known hosts
./resources/restore_known_hosts.sh "$BACKUPTIMESTAMP"

# Restore root's SSH keys
./resources/restore_ssh_keys.sh "$BACKUPTIMESTAMP"

if [ -f "$BACKUPTLSDIR" ]; then
	# Restore the TLS certs
	mkdir -p /etc/gitlab/ssl/
	cd /etc/gitlab/ssl
	cat ${HOME}/${BACKUPTLSDIR} | $DECRYPT_CMD | tar -Jx
fi

# Restart and check GitLab:
gitlab-ctl restart
gitlab-rake gitlab:check SANITIZE=true

# Finally, we run reconfigure to make sure the TLS certs are picked up
# (apparently restarting is not enough)
gitlab-ctl reconfigure
