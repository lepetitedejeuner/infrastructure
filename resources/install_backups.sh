#!/bin/sh -e
# As of this week, do service-specific backups automatically 
# Works on any server
# Usually invoked by vagrant up
#### PREREQUISITES
# - Root's ssh key is in BACKUPUSER@BACKUPHOST's .ssh/authorized_keys
# - backup.sh was copied into /root
# - and scripts backup.sh are copied into the expected locations

DAY=1
HOUR=5
MINUTE=0

if [ "$1" != "" ]; then
	DAY="$1"
fi

if [ "$2" != "" ]; then
	HOUR="$2"
fi

if [ "$3" != "" ]; then
	MINUTE="$3"
fi

printf "Setting up backups for weekly at %d:%02d on $DAY\n" $HOUR $MINUTE
(crontab -l 2>/dev/null; echo "$MINUTE $HOUR * * $DAY /root/backup.sh") | crontab -
