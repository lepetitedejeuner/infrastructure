#!/bin/sh -e
# This script installs nextcloud
# Assumes this will be run as root
# Expects nextcloud_password to be in $HOME
# Takes the hostname it will be accessible by as the only argument.  If
#   nextcloud will only be accessed by IP, then pass in the IP address you
#   will type in to access the service.

if [ -z "$1" ]; then
    echo "Usage: $0 HOSTNAME"
    exit 1
fi
hostname="$1"
if [ ! -f "$HOME/nextcloud_password" ]; then
    echo "Error: $HOME/nextcloud_password not found"
    exit 1
fi

# If there is already a data directory, make sure the permissions are right
if [ -d /var/snap/nextcloud/common/nextcloud ]; then
    chmod 0770 /var/snap/nextcloud/common/nextcloud
fi
umask 007
snap install nextcloud
# If there isn't already an admin user created, add one now
if [ ! -d /var/snap/nextcloud/common/nextcloud/data/admin ]; then
    nextcloud.manual-install admin "`cat $HOME/nextcloud_password`"
fi
nextcloud.occ config:system:set trusted_domains 1 --value=$hostname
