#!/bin/sh -e
# Restore SSH keys from a backup
# Works on any server
# Usually invoked by restore*.sh at deployment time
#### PREREQUISITES
# - Script must be run as root
# - Backed up SSH keypair (2 files in total) is in ~/root
#     - TIMESTAMP-root-id_ed25519[.pub]
# - Timestamp may be specified as the only argument
# - If $HOME/backup_password exists, it will be used to decrypt the backups

cd $HOME
if [ ! -z "$1" ]; then
    TIMESTAMP="$1"
fi
if [ -z "$TIMESTAMP" ]; then
    TIMESTAMP=`ls -tr . | grep -- -root-id_ | tail -n 1 | sed 's/-.*//g'`
    if [ -z "$TIMESTAMP" ]; then
        echo "No SSH keys to restore"
        exit
    fi
    echo "No timestamp given.  Using $TIMESTAMP"
fi

if [ -f "$HOME/backup_password" ]; then
        echo "Password found, backups will be decrypted before being restored"
        DECRYPT_CMD="openssl aes-256-cbc -d -pbkdf2 -pass file:$HOME/backup_password"
else
        echo "No password file found, encryption-at-rest is disabled"
        DECRYPT_CMD="cat"
fi

cd $HOME
if [ -f "$TIMESTAMP-root-id_ed25519" -a -f "$TIMESTAMP-root-id_ed25519.pub" ]; then
    mkdir -p /root/.ssh
    cat "$TIMESTAMP-root-id_ed25519" | $DECRYPT_CMD | cat > /root/.ssh/id_ed25519
    cat "$TIMESTAMP-root-id_ed25519.pub" | $DECRYPT_CMD | cat > /root/.ssh/id_ed25519.pub
    chmod 600 /root/.ssh/id_ed25519*
else
    TIMESTAMP=`ls *-id_ed25519 | tail -n 1 | sed -e 's/-.*//g'`
fi

for privkey in `ls ${TIMESTAMP}-*-id_ed25519`; do
    user=`echo $privkey | sed -e "s/$TIMESTAMP-//" -e "s/-id_ed25519//"`
    if [ -f "$TIMESTAMP-${user}-id_ed25519" -a -f "$TIMESTAMP-${user}-id_ed25519.pub" ]; then
        echo "Restoring key pair for $user from $TIMESTAMP"
        # "eval" needs to be used because the tilde is only expanded for unquoted literals
        cp "$TIMESTAMP-${user}-id_ed25519" $(eval echo ~${user})/.ssh/id_ed25519
        cp "$TIMESTAMP-${user}-id_ed25519.pub" $(eval echo ~${user})/.ssh/id_ed25519.pub
    else
        echo "Unable to restore SSH keys, missing: $TIMESTAMP-${user}-id_ed25519[.pub]"
    fi
done
