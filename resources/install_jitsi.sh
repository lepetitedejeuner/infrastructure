#!/bin/sh -e
# This will install a Jitsi server
# Instructions from multiple sources:
# https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-quickstart
# https://github.com/jitsi/jitsi-meet/issues/6130
export DEBIAN_FRONTEND=noninteractive
FQDN=`hostname -f`

# Dependencies
apt-get install -y apt-transport-https
apt-add-repository universe

# Import their GPG key
wget -q https://download.jitsi.org/jitsi-key.gpg.key
apt-key add jitsi-key.gpg.key
rm jitsi-key.gpg.key

# Install the package
echo "deb https://download.jitsi.org stable/" > /etc/apt/sources.list.d/jitsi-stable.list
apt-get update
echo "jitsi-videobridge jitsi-videobridge/jvb-hostname string $FQDN" | debconf-set-selections
echo "jitsi-meet jitsi-meet/cert-choice select Generate a new self-signed certificate (You will later get a chance to obtain a Let'\''s encrypt certificate)" | debconf-set-selections
apt-get install -qy jitsi-meet
