#!/bin/sh -e
# This script installs nextcloud
# Assumes this will be run as root
# In order to get the TLS cert, the server will need to be internet
# accessible at the hostname specified in the CLI arguments to this script.
#
# If anything is given for the third argument, instead of obtaining a CA signed
# certificate with letsencrypt, a self-signed certificate will be used instead.
# Using a self-signed certificate can also be helpful to use when testing
# changes to the deployment scripts, so you don't run into the limit of certs
# that letsencrypt will issue you per week.  Otherwise you'll have to wait a
# week to continue testing.  I learned this the hard way so you don't have to!

if [ -z "$1" -o -z "$2" ]; then
    echo "Usage: $0 HOSTNAME ADMIN_EMAIL [SELF_SIGNED]"
    exit 1
fi
hostname="$1"
tls_admin="$2"
self_signed="$3"

# Shout out to dbclin for posting instructions
# https://medium.com/@dbclin/administrating-nextcloud-as-a-snap-4eb43ca6d095

# Enable https
# First, we wait for nextcloud to get around to creating the required directory
while [ ! -d /var/snap/nextcloud/current/certs/certbot ]; do
    echo "Sleeping until we have a place to put the TLS certs"
    sleep 5
done
# Then we wait a bit longer, otherwise the nextcloud web server will return a
# 401 unauthorized and cause the process to fail
echo "Wait a minute for things to happen"
sleep 60

# If we do not want a self-signed cert, we use letsencrypt
if [ -z "$self_signed" ]; then
  # We put the "|| true" here because it's okay for this to fail in test
  # environments that are not internet accessible.  We don't want to abort the
  # entire deployment when that happens.
  printf "y\n$tls_admin\n$hostname\n" | nextcloud.enable-https lets-encrypt || true
else
  nextcloud.enable-https self-signed
fi
