#!/bin/sh -e
# Installs djbdns, but does not configure the service (only install)
# Works on any ubuntu server
# Usually invoked by vagrant up
#### PREREQUISITES
# djbdns-1.05.tar.gz is in ~vagrant/
# unattended-upgrade is done running for the moment
# IP is given as the first argument to this script
# zone transfer address/prefix is the second (optional) argument.
#    e.g. 192.168.1. to allow 192.168.1.* to do a zone transfer
# this script is expected to be run as root

if [ `whoami` != "root" ]; then
    echo "Must be run as root.  Exiting."
    exit 1
fi
if [ -z "$1" ]; then
    echo "Usage: $0 IP"
    exit 1
fi
IP="$1"
if [ ! -f ~vagrant/djbdns-1.05.tar.gz ]; then
    echo "Error: ~vagrant/djbdns-1.05.tar.gz not found."
    exit 1
fi

# Make sure we have the add-apt-repository command
sudo apt-get install -y software-properties-common
# Install dependencies
add-apt-repository universe && apt-get update
apt-get install -y daemontools daemontools-run ucspi-tcp build-essential make

# Compile and install djbdns
cd ~vagrant
tar zxf djbdns-1.05.tar.gz
cd djbdns-1.05/
echo gcc -O2 -include /usr/include/errno.h > conf-cc
make
make setup check
## At this point, djbdns is compiled and installed in /usr/local/

# Setting up djbdns users and config files
useradd --no-create-home --shell /bin/false tinydns
useradd --no-create-home --shell /bin/false dnslog
useradd --no-create-home --shell /bin/false axfrdns
tinydns-conf tinydns dnslog /etc/tinydns $IP
axfrdns-conf axfrdns dnslog /etc/axfrdns /etc/tinydns $IP
# Overwrite the rules to allow zone transfers on 192.168.192.* only and
# listen on TCP for everyone
echo "192.168.192.:allow" > /etc/axfrdns/tcp
echo ':allow,AXFR=""' >> /etc/axfrdns/tcp
cd /etc/axfrdns/
make
cd -

# If we had a secondary nameserver, this is where we would modify
# /etc/tinydns/root/Makefile to push the data.cdb file to the remote
# server as described in https://cr.yp.to/djbdns/run-server-bind.html
#
# We should also remember to make sure the remote server's Makefile
# is modified to never overwrite data.cdb (as explained in the URL above)

# Before we start the service, we need to give axfrdns a high memory limit
# Note: the "high" memory limit is 600KB
cat /etc/axfrdns/run | sed -e 's/-d[0-9]*/-d600000/g' > /etc/axfrdns/run.tmp
chmod +x /etc/axfrdns/run.tmp
mv /etc/axfrdns/run.tmp /etc/axfrdns/run

# Now we can start the services
ln -s /etc/tinydns /etc/service/tinydns
ln -s /etc/axfrdns /etc/service/axfrdns
sleep 5
svstat /etc/service/tinydns

# At this point a DNS server should be running, but it doesn't
# know what domain it should do using, what IPs to serve out, etc.
# You will need to create /etc/tinydns/root/data and then run
#   cd /etc/tinydns/root && make
# to set up your server.
