#!/bin/sh -e
# Back up known_hosts file to a remote machine
# Works on any server
# Usually invoked by the guest's backup script
#### PREREQUISITES
# - Script is run as root
# - Root's ssh key is in DESTINATION_HOST's .ssh/authorized_keys
# - DESTINATION_HOST is formatted as user@host (always specify user)
# - Destination host, destination path, and timestamp are given as arguments
# - If $HOME/backup_password exists, backups will be encrypted

TIMESTAMP="$1"
DESTINATION_HOST="$2"
DESTINATION_PATH="$3"
if [ -z "$DESTINATION_HOST" -o -z "$DESTINATION_PATH" -o -z "$TIMESTAMP" ]; then
    echo "Usage: $0 DESTINATION_HOST DESTINATION_PATH TIMESTAMP"
    exit 1
fi

if [ -f "$HOME/backup_password" ]; then
	echo "Password found, encrypting backups before sending them off-site"
	ENCRYPT_CMD="openssl enc -aes-256-cbc -salt -pbkdf2 -pass file:$HOME/backup_password"
else
	echo "No password file found, encryption-at-rest is disabled"
	ENCRYPT_CMD="cat"
fi

cat ~/.ssh/known_hosts | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-root-known_hosts"
