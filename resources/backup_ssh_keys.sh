#!/bin/sh -e
# Back up SSH keys to a remote machine
# Works on any server
# Usually invoked by the guest's backup script
#### PREREQUISITES
# - Root's ssh key is in DESTINATION_HOST's .ssh/authorized_keys
# - If $HOME/backup_password exists, backups will be encrypted

TIMESTAMP="$1"
DESTINATION_HOST="$2"
DESTINATION_PATH="$3"
if [ -z "$TIMESTAMP" -o -z "$DESTINATION_HOST" -o -z "$DESTINATION_PATH" ]; then
    echo "Usage: $0 TIMESTAMP DESTINATION_HOST DESTINATION_PATH"
    exit 1
fi

if [ -f "$HOME/backup_password" ]; then
	echo "Password found, encrypting backups before sending them off-site"
	ENCRYPT_CMD="openssl enc -aes-256-cbc -salt -pbkdf2 -pass file:$HOME/backup_password"
else
	echo "No password file found, encryption-at-rest is disabled"
	ENCRYPT_CMD="cat"
fi

cat ~/.ssh/id_ed25519 | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-root-id_ed25519"
cat ~/.ssh/id_ed25519.pub | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-root-id_ed25519.pub"
