#!/bin/sh -e
export DEBIAN_FRONTEND=noninteractive
export options="-yq"
echo "About to run apt-get update"
apt-get $options update

echo "About to run apt-get upgrade"
apt-get $options upgrade
