#!/bin/sh -e
# Sets up TLS for Jitsi
# Assumes your hostname is set correctly and jitsi is already installed
# Must be run as root
# Usage: $0 ADMIN_EMAIL

echo "$1" | /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
