#!/bin/sh -e
# Script will install telegraf and update settings to point to the correct
# influxdb server.
# - Must be run as root
# - Usage: $0 IP

if [ `whoami` != "root" ]; then
	echo "Must be run as root.  Exiting."
	exit 1
fi
IP="$1"

# Add the repo which has telegraf
curl -sL https://repos.influxdata.com/influxdb.key | apt-key add -
echo "deb https://repos.influxdata.com/debian stretch stable" | tee /etc/apt/sources.list.d/influxdata.list
apt-get update

# Make sure we have the add-apt-repository command
sudo apt-get install -y software-properties-common

# Install it, and lm-sensors so we can get temperature ratings
add-apt-repository universe  # needed for lm-sensors
apt-get install -y telegraf lm-sensors

# Configure it
cd /etc/telegraf
# fix the permissions so it is not world readable
#chown root:telegraf telegraf.conf
#chmod 640 telegraf.conf

p=`cat $HOME/influxdb_password`
sed -i "/^\[\[outputs\.influxdb\]\]/,/^[# ]*\[\[/\
{\
/^# \(^[# ]*\[\[\)/b a;\
s/# *username/username/;\
s/# *password/password/;\
s/password = \".*\"/password = \"$p\"/;\
s/# *urls = \[\"http/urls = \[\"http/;\
s/127.0.0.1/$IP/; :a\
}" telegraf.conf
# Explaination of the madness above:
# sed -i for inline replacement.
# First line of regex:  the following block only applies to things which start 
#                       with [[outputs.influxdb]] and end with [[ (commented out
#                       or not)
# First line in block:  if this line begins with [[ (commented out or not) go to
#                       label "a" this ensures we don't mess with the lines which
#                       start with [[
# Second line in block: Uncomment username
# Third line in block:  Uncomment password
# Fourth line in block: Set password
# Fifth line in block: Set influxdb URL
# Sixth line in block:  Uncomment influxdb URL
#
# Logic taken from: https://stackoverflow.com/questions/29422431/replace-section-of-text-file-using-sed-awk

systemctl restart telegraf
