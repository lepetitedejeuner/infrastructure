#!/bin/sh -e
# Disables systemd's DNS so it doesn't interfere with our own
# Expects to be run with root privs
# resolv.conf must be in $HOME

systemctl stop systemd-resolved
systemctl disable systemd-resolved
if [ -f $HOME/resolv.conf ]; then
	cp $HOME/resolv.conf /etc
else
	echo "#" > /etc/resolv.conf
fi
for arg in $@ ; do
	echo "nameserver $arg" >> /etc/resolv.conf
done
