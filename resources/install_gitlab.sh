#!/bin/bash
# Install gitlab
# Works on any gitlab server
# Usually invoked by vagrant up
#### PREREQUISITES
# - Hostname is set to the correct fully-qualified domain name
# - unattended-upgrade is done running for the moment
# - Must be run as root

FQDN=`hostname`

if [[ "$1" != "" ]]; then
	FQDN="$1"
fi

if [[ "$FQDN" == "" ]]; then
	echo "Could not determine FQDN."
	echo ""
	echo "Usage: $0 [FQDN]"
	echo ""
	exit 1
fi

echo "About to install gitlab dependencies"
apt-get install -y curl openssh-server ca-certificates rsync
debconf-set-selections <<< "postfix postfix/mailname string $FQDN"
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
apt-get install -y postfix

# Make sure we don't get an "encoding mismatch" error from postgres
export LANG="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
# We use the omnibus installation: https://about.gitlab.com/installation/#ubuntu
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | bash
EXTERNAL_URL="http://$FQDN" apt-get install -y gitlab-ee
