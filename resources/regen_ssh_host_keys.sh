#!/bin/sh -e
rm /etc/ssh/ssh_host_*
dpkg-reconfigure --frontend=noninteractive openssh-server
systemctl restart ssh
