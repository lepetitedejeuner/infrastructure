#!/bin/sh -e
# Works on any ubuntu server
# Usually invoked by vagrant up
# this script is expected to be run as root
#### PREREQUISITES
# djbdns is installed in the standard location
# data file has been copied to ~root/
mv ~/data /etc/tinydns/root/
cd /etc/tinydns/root && make
