#!/bin/sh -e
# Backup host keys
# Works on any server
# Usually invoked by the guest's backup script
#### PREREQUISITES
# - Root's ssh key is in DESTINATION_HOST's .ssh/authorized_keys
# - If $HOME/backup_password exists, backups will be encrypted

TIMESTAMP="$1"
DESTINATION_HOST="$2"
DESTINATION_PATH="$3"
if [ -z "$TIMESTAMP" -o -z "$DESTINATION_HOST" -o -z "$DESTINATION_PATH" ]; then
    echo "Usage: $0 TIMESTAMP DESTINATION_HOST DESTINATION_PATH"
    exit 1
fi

if [ -f "$HOME/backup_password" ]; then
	echo "Password found, encrypting backups before sending them off-site"
	ENCRYPT_CMD="openssl enc -aes-256-cbc -salt -pbkdf2 -pass file:$HOME/backup_password"
else
	echo "No password file found, encryption-at-rest is disabled"
	ENCRYPT_CMD="cat"
fi

cat /etc/ssh/ssh_host_ecdsa_key | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-ssh_host_ecdsa_key"
cat /etc/ssh/ssh_host_ecdsa_key.pub | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-ssh_host_ecdsa_key.pub"
cat /etc/ssh/ssh_host_ed25519_key | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-ssh_host_ed25519_key"
cat /etc/ssh/ssh_host_ed25519_key.pub | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-ssh_host_ed25519_key.pub"
cat /etc/ssh/ssh_host_rsa_key | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-ssh_host_rsa_key"
cat /etc/ssh/ssh_host_rsa_key.pub | $ENCRYPT_CMD | ssh "$DESTINATION_HOST" "cat > $DESTINATION_PATH/$TIMESTAMP-ssh_host_rsa_key.pub"
