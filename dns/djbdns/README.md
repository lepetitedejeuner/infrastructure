# Non-resoling DNS server
This VM sets up DJBDNS as a non-resolving name server.  This means it only
answers DNS queries for your domain (example.com in this example).  This is in
contrast to a "normal" DNS server which will look up the IP address of any
hostname.

There is also a script that backs up SSH host keys, SSH keys and known hosts.
If the backed up files a put in the same directory as the Vagrantfile, they
will be used when deploying the VM.  This allows you to have stable SSH host
keys, which means less manual verification of key fingerprints.  It makes sure
that, if you've already trusted the host keys of the backup server, you won't
need to do that again.  Finally, it means you won't need to update the public
SSH key on the backup server each time you redeploy.

# Customization
To try this one out for your domain, you'll want to:
- [Do the normal things](../../README.md#Customization)
- Change the zone file (data)

# Testing
Scripts tested with:
- Ubuntu 18.04
- Ubuntu 20.04

# Opportunities for improvment
The backup process and SSH keys are cumbersome.  There are a number of ways to
address this, but setting those up go beyond the scope of this simple example.
