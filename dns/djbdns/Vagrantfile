# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure("2") do |config|
  #config.vm.box = "generic/ubuntu1804"
  config.vm.box = "generic/ubuntu2004"
  hostname = "ns2.example.com"
  ip = "192.168.0.200"
  gateway = "192.168.0.1"
  dns = "4.2.2.2"

  config.vm.network "public_network", ip: ip, bridge: ["en0", "enp4s0", "wlp1s0"]
  config.vm.host_name = hostname
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provider "virtualbox" do |vb|
     vb.memory = "512"
     vb.linked_clone = true
     vb.name = hostname
  end

  config.vm.provision "file", source: "../../resources",
                              destination: "$HOME/resources"
  config.vm.provision "shell", privileged: false,
                               inline: "sudo mv resources ~root/"

  config.vm.provision "shell", path: "../../resources/fix_routes.sh",
                               run: "always", name: "fix_routes",
                               args: [ "-g", gateway, "-d", dns ]
  config.vm.provision "shell", path: "../../resources/update.sh", name: "update"
  config.vm.provision "shell", path: "../../resources/regen_ssh_host_keys.sh",
                               name: "regen_ssh_host_keys"

  # If we have backed up host keys, restore them
  files = Dir.entries(".")
  files.each { |f|
    if (f.include?("ssh_host") or f.include?("id_ed25519") or
        f.include?("known_hosts") or f == "backup_password")
      config.vm.provision "file", source: f, destination: f
      config.vm.provision "shell", inline: "sudo mv #{f} ~root/", privileged: false
    end
  }
  config.vm.provision "shell", path: "../../resources/restore_host_keys.sh",
                               name: "restore_host_keys"
  config.vm.provision "shell", path: "../../resources/restore_ssh_keys.sh",
                               name: "restore_ssh_keys"
  config.vm.provision "shell", path: "../../resources/restore_known_hosts.sh",
                               name: "restore_known_hosts"

  # Install djbdns and dependencies
  config.vm.provision "file", source: "../../resources/djbdns-1.05.tar.gz",
                               destination: "djbdns-1.05.tar.gz"
  config.vm.provision "shell", path: "../../resources/install_djbdns.sh",
                               args: [ip], name: "install_djbdns"

  # Set up djbdns
  config.vm.provision "file", source: "data", destination: "data"
  config.vm.provision "shell", privileged: false, inline: "sudo mv data ~root/"
  config.vm.provision "shell", path: "../../resources/set_up_djbdns.sh",
                               name: "set_up_djbdns"

  # Make sure we're backing up everything weekly
  config.vm.provision "file", source: "backup.sh", destination: "backup.sh"
  config.vm.provision "shell", privileged: false,
                               inline: "sudo mv backup.sh ~root/"
  config.vm.provision "shell", path: "../../resources/install_backups.sh",
                               args: ["1", "2", "55"],
                               name: "install_backups" # Monday @ 02:55

  # Add my SSH key to the machine so I can SSH into it
  config.vm.provision "shell", path: "../../resources/configure_ssh_user_keys.sh",
                               args: ["alice"]
end
