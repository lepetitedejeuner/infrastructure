#!/bin/sh -e
# Back up to a remote machine
# install_backups.sh usually installs this script in /root
# Usually invoked by root's crontab
#### PREREQUISITES
# - All vagrant provisioning is complete
# - Root's ssh key is in .ssh/authorized_keys on the backup server

DESTINATION_HOST=vagrant@backup.example.com
DESTINATION_PATH=/mnt/backup/ns1
TIMESTAMP=`date +%Y.%m.%d`

/root/resources/backup_host_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_ssh_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_known_hosts.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH

