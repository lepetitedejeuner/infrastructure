#!/bin/sh -e
# This script restores all the data for grafana
# It assumes it is run as root, that all of the software is already installed,
# and that the backups are located in $HOME
# The optional argument is IP address, which is only really important when the
# IP address is changing, as the backed up config file should already have the
# correct address.

cd $HOME
TIMESTAMP=`ls *-grafana.* | tail -n 1 | sed -e 's/-grafana.*//g'`
if [ -z "$TIMESTAMP" ]; then
	echo "No files to restore"
	exit 0
fi

if [ -z "$1" ]; then
	echo "Usage: $0 TIMESTAMP IP"
	echo "    TIMESTAMP - The timestamp of the backup"
	echo "    IP - The IP address to set in influxdb.conf"
	exit 1
fi
IP=""
if [ -n "$2" ]; then
	IP="$2"
fi

# Restore config files & database
if [ -f ${TIMESTAMP}-grafana.ini ]; then
	echo "Restoring configuration file"
	cp ${TIMESTAMP}-grafana.ini /etc/grafana/grafana.ini
	chown grafana:grafana /etc/grafana/grafana.ini
	if [ -n "$IP" ]; then
		sed -i "s/http_addr = .*/http_addr = $IP/g" /etc/grafana/grafana.ini
	fi
fi
if [ -f ${TIMESTAMP}-grafana.db ]; then
	echo "Restoring database"
	cp ${TIMESTAMP}-grafana.db /var/lib/grafana/grafana.db
	chown grafana:grafana /var/lib/grafana/grafana.db
fi

# And start grafana now that we have all the configs & data in place
/bin/systemctl start grafana-server

# If the backup doesn't already have this patch, we'll need it.  Next we need
# to set up nginx to point to grafana so requests to port 443 get to grafana
# (which runs on TCP/3000).
cd /etc/nginx/sites-available
grep 'location / ' default
if [ ! $? -eq 0 ]; then
	echo "Proxy for / not found in nginx config, patching..."
	patch < ~root/add_grafana_to_zm_config.patch
fi
systemctl restart nginx grafana-server
