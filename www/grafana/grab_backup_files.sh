#!/bin/sh
# This will scp the backup files over from the backup server
# Defaults to pulling today's backups, override by passing in a prefix as the
# first argument.
DESTINATION_HOST="$1"
if [ -n "$2" ]; then
	prefix="$2"
else
	prefix=`date +%Y.%m.%d`
fi

h=`grep "hostname =" Vagrantfile | sed -e 's/"$//g' -e 's/.*"//g'`
scp $DESTINATION_HOST:/mnt/backup/$h/${prefix}* .
