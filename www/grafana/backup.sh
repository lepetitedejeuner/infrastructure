#!/bin/sh -e
# This script is to be run from the server.  It will back everything
# up and then copy it to the appropriate backup location.
DESTINATION_HOST="vagrant@backup.example.com"
DESTINATION_PATH="/mnt/backup/`hostname -f`"
TIMESTAMP=`date +%Y.%m.%d`

/root/resources/backup_host_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_ssh_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_known_hosts.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH

echo "Backups will be copied to $DESTINATION_HOST:$DESTINATION_PATH"
scp /var/lib/grafana/grafana.db $DESTINATION_HOST:$DESTINATION_PATH/$TIMESTAMP-grafana.db
scp /etc/grafana/grafana.ini $DESTINATION_HOST:$DESTINATION_PATH/$TIMESTAMP-grafana.ini
