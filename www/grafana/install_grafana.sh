#!/bin/sh -e
# This script installs grafana and nginx so you can get to the web server on
# port 80.
# Assumes this will be run as root
# Assumes nginx is installed and is or will be set up to proxy requests to 443
#   on the host over to the grafana server (port 3000)

# Get dependencies, add vendor's PGP key, add the repo, and install from there
apt-get install -y apt-transport-https software-properties-common wget
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
apt-get update
apt-get install -y grafana nginx

# Make sure grafana starts on boot, and start the service
systemctl enable grafana-server
systemctl start grafana-server

# Copy over the nginx config and restart the service
if [ -f $HOME/default ]; then
	mv $HOME/default /etc/nginx/sites-available/
fi
systemctl restart nginx
