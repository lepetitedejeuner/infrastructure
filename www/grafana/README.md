# Grafana
Grafana lets you visualize your data.  It's used for many things ranging from
tracking temperature, humidity, and barometric pressure over time, to
monitoring and alerting when servers have a problem.  You can see it in graphs,
charts, gauges, or a variety of other ways.

This example works with the [influxdb](../influxdb) and
[backup](../../ssh/backup) examples.  Grafana needs to connect to some data
source so it has data to show you.  In this example, that's influxdb.

However, that data source also needs to be populated by something in order for
there to be anything meaningful to see.  In this example, we have a program
called telegraf installed on a Linux machine (backup) that will connect to the
influxdb server and record server stats such as CPU usage, number of processes,
memory usage and so forth.  This give us some data that we can visualize.

# Customization
To try this one out for your domain, you'll want to:
- [Do the normal things](../../README.md#Customization)

# Configuration
The default username/password is admin/admin.  Password change is forced on
first login.

If you have the influxdb VM deployed, you can use that as your data source for
grafana.  To do so, hover over the gear icon on the left of Grafana's web UI,
select data sources, add data source, and select Influx DB.  The URL will be
http://192.168.0.204:8086 (possibly a different IP address, if the default
value was not used).  For the influxdb details:

- database is telegraf
- username is telegraf
- password is whatever was in the influxdb_password file when influxdb was
  deployed

Click "save and test" to verify everything is working as expected.

Afer you have a datasource, it's time to set up a dashboard to look at the data
in the database.  You can build your own dashboard, but to get up and running
quickly, we're going to import one that someone else already built.

On the left of the Grafani UI, hover over the icon that is four squares
(Dashboards) and click Manage.  Click import, enter ID
[11912](https://grafana.com/grafana/dashboards/11912), click load, then
select the influxdb data source (the only option in the drop down list), and
click Import.

Now you should see your new Grafana dashboard.

# Troubleshooting
If you are now able to connect to the influxdb server in grafana, try to do so
via the command line.

```bash
sudo apt-get install -y influxdb-client
influx -host 192.168.0.204 -port 8086 -database telegraf -username telegraf -password whatever
```

If it is working properly, you should see a prompt like this:

```
Visit https://enterprise.influxdata.com to register for updates, InfluxDB server management, and monitoring.
Connected to http://192.168.0.204:8086 version 1.8.2
InfluxDB shell version: 1.1.1
> 
```

To confirm queries can be run, try something like this:

```
SELECT * FROM processes LIMIT 10;
```

You should get query results.  If you get `ERR: authorization failed`, that
will tell you there's something wrong with the user.  At that point, the next
step would be to troubleshoot on the influxdb side.
