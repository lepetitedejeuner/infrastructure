#!/bin/sh -e
# This script restores all the data for nextcloud
# It assumes it is run as root, that all of the software is already installed,
# and that the backups are located in $HOME

cd $HOME
TIMESTAMP=`ls *-nextcloud-config.tar.gz | tail -n 1 | sed -e 's/-nextcloud-config.tar.gz//g'`
# Not sure why waiting is necesary, but it is... :-/
echo "Wait a minute for things to calm down, or whatever"
sleep 60
echo "Restoring from backup on $TIMESTAMP"

# Normally we would run nextcloud.occ maintenance:mode --on
# first, but immediately after an install, that command is not available
# If you are hacking up this script to be run on an instance that is already
# set up, you'll want to add a line to go into maintenance mode

# Restore the data
cd /var/snap/nextcloud/common
tar -xzf $HOME/$TIMESTAMP-nextcloud-data.tar.gz
rm $HOME/$TIMESTAMP-nextcloud-data.tar.gz

# restore the configs
cd /var/snap/nextcloud/current/nextcloud
tar --overwrite -xzf $HOME/$TIMESTAMP-nextcloud-config.tar.gz
rm $HOME/$TIMESTAMP-nextcloud-config.tar.gz

# Restore the database
nextcloud.mysql-client nextcloud < $HOME/$TIMESTAMP-nextcloud.sql
rm $HOME/$TIMESTAMP-nextcloud.sql

# The mysql database gets a different password every time.  This password is
# stored in multiple locations and the one in the config.php file we restored
# is wrong.  Luckily, there's a file with the new (correct) database password,
# that we can use to patch up our config file.
cd /var/snap/nextcloud/current/nextcloud/config
sed -i "s/'dbpassword' => '.*/'dbpassword' => '`cat /var/snap/nextcloud/current/mysql/nextcloud_password`',/" config.php
snap restart nextcloud

# We never went into maintenance mode, so we never have to come out of it
#nextcloud.occ maintenance:mode --off
echo "Done restoring from backup"
