#!/bin/sh -e
# This file just shows how to manually upgrade Nextcloud, but the script isn't
# actually used in any cron jobs (to avoid upgrades without having a backup
# done immediately before attempting to do the upgrade).
#
# However, Nextcloud seems to upgrade itself automatically and the upgrades
# seem to be well tested enough that they don't really cause any problems.
#
# Instructions taken from:
# https://docs.nextcloud.com/server/18/admin_manual/maintenance/package_upgrade.html
#
sudo snap refresh nextcloud
sudo nextcloud.occ upgrade
