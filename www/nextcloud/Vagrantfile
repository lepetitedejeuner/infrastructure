# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure("2") do |config|
  config.vm.box = "generic/ubuntu1804"
  hostname = "nextcloud.example.com"
  ip = "192.168.0.201"
  gateway = "192.168.0.1"
  dns = "4.2.2.2"
  tls_admin = "admin@example.com"

  # Create a public network, which generally matched to bridged network.
  config.vm.network "public_network", ip: ip, bridge: ["en0", "enp4s0", "eno1", "wlp1s0"]
  config.vm.host_name = hostname
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # Increase the size of the virtual disk if we have the plugin. To get it:
  #   vagrant plugin install vagrant-disksize
  if Vagrant.has_plugin?("vagrant-disksize")
    config.disksize.size = '100GB'
  end

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  config.vm.provider "virtualbox" do |vb|
     vb.cpus = "2"
     vb.memory = "2048"
     vb.linked_clone = true
     vb.name = hostname
  end

  config.vm.provision "file", source: "../../resources",
                              destination: "$HOME/resources"
  config.vm.provision "shell", privileged: false,
                               inline: "sudo mv resources ~root/"

  config.vm.provision "shell", path: "../../resources/fix_routes.sh",
                               run: "always", name: "fix_routes",
                               args: [ "-g", gateway, "-d", dns ]
  config.vm.provision "shell", path: "../../resources/update.sh", name: "update"
  config.vm.provision "shell", path: "../../resources/regen_ssh_host_keys.sh",
                               name: "regen_ssh_host_keys"

  # If we have backed up host keys, restore them
  files = Dir.entries(".")
  files.each { |f|
    if (f.include?("ssh_host") or f.include?("-root-id") or
        f.include?("known_hosts") or f == "backup_password")
      config.vm.provision "file", source: f, destination: f
      config.vm.provision "shell", inline: "sudo mv #{f} ~root/", privileged: false
    end
  }
  config.vm.provision "shell", path: "../../resources/restore_host_keys.sh",
                               name: "restore_host_keys"
  config.vm.provision "shell", path: "../../resources/restore_ssh_keys.sh",
                               name: "restore_ssh_keys"
  config.vm.provision "shell", path: "../../resources/restore_known_hosts.sh",
                               name: "restore_known_hosts"

  # Put password file in root's home directory
  config.vm.provision "file", source: "nextcloud_password",
                              destination: "$HOME/nextcloud_password"
  config.vm.provision "shell", privileged: false,
                              inline: "sudo mv nextcloud_password ~root/"
  # Install nextcloud
  config.vm.provision "shell", path: "../../resources/install_nextcloud.sh",
                               args: [hostname], name: "install nextcloud"
  config.vm.provision "shell", path: "../../resources/enable_tls_nextcloud.sh",
                               args: [hostname, tls_admin, "self-signed"],
                               name: "TLS nextcloud"

  # Next, we restore all our configs & data
  files = Dir.entries(".")
  we_found_something_to_restore = false
  files.each { |f|
    if f.include?(".tar.gz") || f.include?(".sql")
      config.vm.provision "file", source: f, destination: f
      config.vm.provision "shell", inline: "sudo mv #{f} ~root/",
                                   privileged: false
      we_found_something_to_restore = true
    end
  }
  if we_found_something_to_restore
    config.vm.provision "shell", path: "restore.sh", name: "restore.sh"
  end

  # Install backup script and cron job to backup every Mon @ 05:15
  config.vm.provision "file", source: "backup.sh", destination: "backup.sh"
  config.vm.provision "shell", privileged: false,
                               inline: "sudo mv backup.sh ~root/"
  config.vm.provision "shell", path: "../../resources/install_backups.sh",
                               args: ["1", "5", "15"],
                               name: "install_backups" # Monday @ 05:15

  # Add my SSH key to the machine so I can SSH into it
  config.vm.provision "shell", path: "../../resources/configure_ssh_user_keys.sh",
                               args: ["alice"]
end
