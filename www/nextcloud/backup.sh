#!/bin/sh -e
# This will back up the nextcloud server. It needs to be run as root
#
# Shout out to the authors of:
# https://help.nextcloud.com/t/nextcloud-snap-question-about-nextcloud-mysql-client/30275
# https://www.addictivetips.com/ubuntu-linux-tips/backup-nextcloud-snap-installation-on-linux/
#
# And the official docs on backing things up:
# https://docs.nextcloud.com/server/18/admin_manual/maintenance/backup.html
#
# If we want to put the data directory on external media in the future, this might help:
# https://github.com/nextcloud/nextcloud-snap/issues/150#issuecomment-267641192
#
# Oh, and if you want to know more about this whole "snap" thing, check this:
# https://www.freecodecamp.org/news/managing-ubuntu-snaps/
#
DESTINATION_HOST="vagrant@backup.example.com"
DESTINATION_PATH="/mnt/backup/`hostname -f`"
TIMESTAMP=`date +%Y.%m.%d`

# First, we back up the normal stuff
/root/resources/backup_host_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_ssh_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_known_hosts.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH

# Then we back up the nextcloud data
# Enable maintenance mode
nextcloud.occ maintenance:mode --on

# Dump the data directory
cd /var/snap/nextcloud/common
tar cz . | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/${TIMESTAMP}-nextcloud-data.tar.gz"
cd -

# Dump the database
nextcloud.mysqldump | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/${TIMESTAMP}-nextcloud.sql"

# Grab the configs
cd /var/snap/nextcloud/current/nextcloud
tar cz . | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/${TIMESTAMP}-nextcloud-config.tar.gz"

# Take it back out of maintenance mode
nextcloud.occ maintenance:mode --off
