#!/bin/sh -e
# This will scp the backup files over from the backup server.  Run this before
# you deploy if you want to pull everything in.
# Defaults to pulling today's backups, override by passing in a prefix as the
# second argument.
#
# Usage: $0 [user@]BACKUPHOST [PREFIX]
#
#### PREREQUISITES
# Run as the user that deploys & runs the VM
# Backup user is able to SSH into the backup server
# Script is run from the directory with the Vagrantfile

DESTINATION_HOST="$1"
if [ -n "$2" ]; then
	prefix="$2"
else
	prefix=`date +%Y.%m.%d`
fi

h=`grep "hostname =" Vagrantfile | sed -e 's/"$//g' -e 's/.*"//g'`
scp $DESTINATION_HOST:/mnt/backup/${h}/${prefix}* .
