# Nextcloud server
Nextcloud was originally a server that let you get files from your mobile
devices to your computer, share files with others.  Now it is that and quite
a bit more.  It has support for syncing contacts, calendars, and bookmarks. It
has a text chat system and videoconferencing.  It can replace a lot of the
services people use.

Why spend the hassle of running your own server instead of just using other
companies?
- They might look at your data so they can feed you more relevant advertisments
- They might go out of business and all your files could be gone
- If the server is down, you can fix it
- Privacy law requires you to guarantee that you keep customers' data in a
  specific region, country or state
- You simply like the idea of self reliance
- You want to offer the benefits above to your friends and family


# Passwords
The passwords for the admin account of nextcloud is stored in files in the
deployment directory on the host.  It is used as part of the deployment so the
server can be set up.  The file should only contain the password on a line by
itself, and they should be named nextcloud_password.  Normally this file would
never be committed to a repo, but this is a simple example that we wanted to
work out of the box.

A safer way to store this would be to put the password in your password
database and then create the password file when you deploy, never commiting it
to any git repo.  There are more complicated setups such as using a password
vault (e.g. [https://www.vaultproject.io/](Vault)) that is used as part of a
larger deployment setup.  That kind of a deployment is beyond the scope of
these examples (for now).

Note: the admin password will be overwritten if you restore from a backup.  In
other words, when you restore from a backup, the admin password will be
whatever it was when you took the backup.  However, the nextcloud_password file
is still required to get the system set up to the point where restoration is
possible.

# Customization
To try this one out for your domain, you'll want to:
- [Do the normal things](../../README.md#Customization)
- Replace nextcloud_passport with your own password ([https://www.youtube.com/watch?v=-zVgWpVXb64](Reference))

# Testing
Scripts tested with:
- Ubuntu 18.04
- Ubuntu 20.04

# Troubleshooting
Using journalctl -e is useful when things are really bad.  Aside from there,
https://github.com/nextcloud/nextcloud-snap/wiki/Where-to-find-logs-of-components
has some other places where you can find log files:

Logs for Nextcloud can be found at:
/var/snap/nextcloud/common/nextcloud/data/nextcloud.log

Logs for Apache can be found at: /var/snap/nextcloud/current/apache/logs/

Logs for Redis can be found at: /var/snap/nextcloud/current/redis/redis.log

Logs for PHP-FPM can be found at: /var/snap/nextcloud/current/php/php-fpm.log

Logs for Certbot can be found at:
/var/snap/nextcloud/current/certs/certbot/logs/letsencrypt.log

Logs for MySQL can be found at:
/var/snap/nextcloud/current/mysql/localhost.localdomain.err
