#!/bin/sh -e
# We are not currently using this script because the intallation script that we
# are using from the jitsi project doesn't check to see if TLS certs already
# exist.
#
# If we want to use this, we will need to reproduce the things that are done by
# /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh (for more details,
# see ../../resources/enable_tls_jitsi.sh).
#
echo "Restoring TLS certs"
mkdir -p /etc/letsencrypt  # Allows restoring certs before installing certbot
cd /etc/letsencrypt
tar Jxf $HOME/*-letsencrypt.tar.xz
rm $HOME/*-letsencrypt.tar.xz

mkdir -p /etc/coturn
cd /etc/coturn
tar Jxf $HOME/*-coturn_certs.tar.xz
rm $HOME/*-coturn_certs.tar.xz

echo "Done with $0"
