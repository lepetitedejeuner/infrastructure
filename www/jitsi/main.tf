variable "fqdn" { default = "jitsi.example.com" }
variable "backup_user" { default = "backup" }
variable "backup_server" { default = "backup.example.com" }
locals { ssh_privkey = file(pathexpand("~/.ssh/id_ed25519")) }

module "standard_vm" {
  source = "./../../tf_standard_vm"
  fqdn = var.fqdn
  ip_address = "192.168.1.62" # Duplicate in provisioning step
  memory = 4096
  cores = 6
  sshkeys = file("${path.root}/../../resources/alice.pub")
}

# Provision our VM after we have our updates done and backups restored
# https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource
resource "null_resource" "provisioning" {
  depends_on = [ module.standard_vm ]
  # Connection info for all of the provisioners to use
  connection {
    type = "ssh"
    host = "192.168.1.62"
    user = "root"
    private_key = file(pathexpand("~/.ssh/id_ed25519"))
  }

  # Copy the config file over to be restored
  provisioner "file" {
    source = "jitsi.example.com.conf"
    destination = "jitsi.example.com.conf"
  }
  # Install everything
  provisioner "remote-exec" {
    scripts =  [ "../../resources/install_jitsi.sh",
                 "../../resources/enable_tls_jitsi.sh" ]
  }
  provisioner "remote-exec" {
    when   = destroy
    inline = [ "/root/backup.sh" ]
  }
}

# Once we're provisioned, make sure we are taking regular backups
module "backups" {
  depends_on = [ null_resource.provisioning ]
  source = "./../../tf_backups"
  ip_address = module.standard_vm.ip_address
  backup_user = var.backup_user
  backup_server = var.backup_server
  ssh_privkey = local.ssh_privkey
}
