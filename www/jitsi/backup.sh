#!/bin/sh -e
#
# This script is to be run from the jitsi server and it will back up
# everything and then copy it to the appropriate backup location.
#
DESTINATION_HOST="backup@backup.`hostname -d`"
DESTINATION_PATH="/var/backups/`hostname -f`"
TIMESTAMP=`date +%Y.%m.%d`

# Make sure the destination directory exists
ssh $DESTINATION_HOST "mkdir -p $DESTINATION_PATH"

/root/resources/backup_host_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_ssh_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_known_hosts.sh $DESTINATION_HOST $DESTINATION_PATH $TIMESTAMP

# Backup all the TLS certificates
cd /etc/letsencrypt
tar -Jc accounts live archive renewal | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/$TIMESTAMP-letsencrypt.tar.xz"
cd /etc/coturn
tar -Jc certs | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/$TIMESTAMP-coturn_certs.tar.xz"
