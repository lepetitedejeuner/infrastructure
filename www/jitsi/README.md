# Jitsi
Jitsi serves up a webpage on 443 and that is used for joining a videoconference
room, text chat, muting other people and so forth.  It does NOT handle the
audio and video traffic.  That goes to the jitsi video bridge (jvb) and it uses
UDP/10000.

When only two people are in a conference room, audio and video doesn't go
through the server, it goes directly between the clients.  If a third person
joins, it will automatically switch to route all the traffic through the
server.

# Deployment
When deploying this will ALWAYS get a new certificate from letsencrypt.  This
is because we are using jitsi's installation scripts.  We only get something
like 5 per week, so be very careful when testing, otherwise we risk having a
broken jitsi server for a week.

# TURN server
For people who can not use UDP/10000, clients can fall back to using a TURN
server.  Currently we do not have a TURN server, and we are not using a third
party TURN server.  TURN servers will proxy all traffic between clients and the
jvb service.  I'm not sure if the TURN server can decrypt the DTLS on the video
packets.  I believe the answer is yes because it converts from TCP to UDP.

This means that people can not access our server over Tor, which only supports
TCP.

In /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh there is logic to
update the TURN server configuration if we already have one installed.

Resources:
- https://community.jitsi.org/t/jitsi-meet-coturn/82394/9
- https://community.jitsi.org/t/coturn-chronicles/73099
- https://gabrieltanner.org/blog/turn-server
