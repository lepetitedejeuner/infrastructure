#!/bin/sh -e
# Set the user account and systemd script to run the service
user="matrix"

useradd --create-home --shell /bin/bash $user

export DEBIAN_FRONTEND=noninteractive
apt install -yq postgresql nginx python3-certbot-nginx

# Get via .deb packages from matrix.org's repo
# The distro repos are not used, as they reportedly contain outdated,
# vulnerable versions of the server software.
# Ref: https://github.com/matrix-org/synapse/blob/master/INSTALL.md#downstream-debian-packages
# Ref: https://github.com/matrix-org/synapse/blob/master/INSTALL.md#downstream-ubuntu-packages
apt install -y lsb-release wget apt-transport-https
wget -O /usr/share/keyrings/matrix-org-archive-keyring.gpg https://packages.matrix.org/debian/matrix-org-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/matrix-org-archive-keyring.gpg] https://packages.matrix.org/debian/ $(lsb_release -cs) main" > /etc/apt/sources.list.d/matrix-org.list
apt update
echo matrix-synapse-py3 matrix-synapse/server-name select `hostname -d` | debconf-set-selections
apt install -yq matrix-synapse-py3

# Database configuration
# We use a random password for every installation and replace slashes with
# underscores so it doesn't cause problems when embedding the password into
# a URI (for the database)
pw=`dd if=/dev/urandom bs=16 count=1 2>/dev/null | base64 | sed 's|/|_|g'`
echo "create user synapse password '$pw'" | sudo -u postgres psql
sudo -u postgres createdb -O synapse synapse
echo "UPDATE pg_database SET datcollate='C', datctype='C', encoding=pg_char_to_encoding('UTF8') WHERE datname='synapse'" | sudo -u postgres psql

if [ -f $HOME/synapse.pgdump ]; then
	echo "Restoring SQL database from backup file"
	sudo -u postgres psql synapse < $HOME/synapse.pgdump
fi

# synapse configuration
sed -i "s/server_name:.*/server_name: `hostname -d`/g" /etc/matrix-synapse/conf.d/server_name.yaml
if [ -f $HOME/homeserver.yaml ]; then
	echo "Restoring homeserver configuration file"
	mv $HOME/homeserver.yaml /etc/matrix-synapse
fi
# Set a fresh macaroon password
sed -i "s/password:.*/password: $pw/g" /etc/matrix-synapse/homeserver.yaml
s=`dd if=/dev/urandom bs=32 count=1 2>/dev/null | base64 | sed 's|/|_|g'`
sed -i "s/registration_shared_secret:.*/registration_shared_secret: $s/g" /etc/matrix-synapse/homeserver.yaml

# Restore signing key if we have one
if [ -f $HOME/homeserver.signing.key ]; then
	echo "Restoring matrix_key from backups"
	chown matrix-synapse:nogroup $HOME/homeserver.signing.key
	chmod 600 $HOME/homeserver.signing.key
	mv $HOME/homeserver.signing.key /etc/matrix-synapse
fi
if [ -f $HOME/media.tar.xz ]; then
	echo "Restoring media files"
	cd /var/lib/matrix-synapse/media
	tar Jxf $HOME/media.tar.xz
fi

# Get TLS certs (if we didn't restore them from backup) and set up cron job to
# pull them regularly
chmod +x $HOME/set_up_letsencrypt.sh
$HOME/set_up_letsencrypt.sh --nginx -d `hostname -f` --email "admin@`hostname -d`"
rm $HOME/set_up_letsencrypt.sh
# Make sure the matrix user can get to the certificates
chown -R $user /etc/letsencrypt/live
chown -R $user /etc/letsencrypt/archive

# The TLS entry for nginx proxy /_matrix to the server
sed -i 's|location / {|location /_matrix {|g' /etc/nginx/sites-available/default
# Security headers
sed -i '/^[^#].*location \/_matrix {/i add_header X-Frame-Options SAMEORIGIN;' /etc/nginx/sites-available/default
sed -i '/^[^#].*location \/_matrix {/i add_header X-Content-Type-Options nosniff;' /etc/nginx/sites-available/default
sed -i '/^[^#].*location \/_matrix {/i add_header X-XSS-Protection "1; mode=block";' /etc/nginx/sites-available/default
sed -i "/^[^#].*location \/_matrix {/i add_header Content-Security-Policy \"frame-ancestors 'none'\";" /etc/nginx/sites-available/default
# Proxy settings
sed -i '/^[^#].*location \/_matrix {/i proxy_set_header X-Forwarded-For $remote_addr;' /etc/nginx/sites-available/default
sed -i '/^[^#].*location \/_matrix {/i proxy_set_header X-Forwarded-Proto $scheme;' /etc/nginx/sites-available/default
sed -i 's|try_files.*|proxy_pass https://localhost:8448;|g' /etc/nginx/sites-available/default
# Allow people to upload images/videos over 1M (default)
sed -i '/^[^#].*location \/_matrix {/i client_max_body_size 50M;' /etc/nginx/sites-available/default

# Finally, if we have a config file to restore, we use that to overwrite
# everything we just set up
if [ -f $HOME/default ]; then
	mv $HOME/default /etc/nginx/sites-available/default
fi

# Make sure the landing page on the website does not throw an error 401
if [ -f $HOME/index.html ]; then
	mv $HOME/index.html /var/www/html/
else
	echo "<html></html>" > /var/www/html/index.html
fi

# Restart nginx and matrix to pick up the updated configs
systemctl restart nginx matrix-synapse
