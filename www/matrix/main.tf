variable "fqdn" { default = "matrix.example.com" }
variable "backup_server" { default = "backup.example.com" }
variable "target_node" { default = "thyme" }
locals { ssh_privkey = file(pathexpand("~/.ssh/id_ed25519")) }

module "standard_vm" {
  source = "./../../tf_standard_vm"
  fqdn = var.fqdn
  cores = 2
  memory = 2048
  target_node = var.target_node
  backup_server = var.backup_server
  ip_address = "192.168.1.111" # Duplicate in provisioning step
  sshkeys = join("", [file("${path.root}/../../resources/alice.pub"),
                      file("${path.root}/../../resources/bob.pub")])
}

resource "null_resource" "provisioning" {
  depends_on = [ module.standard_vm ]
  # Connection info for all of the provisioners to use
  connection {
    type = "ssh"
    host = "192.168.1.111"
    user = "root"
    private_key = file(pathexpand("~/.ssh/id_ed25519"))
  }

  provisioner "file" {
    source = "${path.root}/matrix.service"
    destination = "matrix.service"
  }
  provisioner "file" {
    source = "${path.root}/default"
    destination = "default"
  }
  provisioner "file" {
    source = "${path.root}/../../resources/set_up_letsencrypt.sh"
    destination = "set_up_letsencrypt.sh"
  }
  provisioner "file" {
    source = "${path.root}/index.html"
    destination = "index.html"
  }
  provisioner "remote-exec" {
    script =  "setup.sh"
  }
  provisioner "remote-exec" {
    when   = destroy
    inline = [ "/root/backup.sh" ]
  }
}

# Make sure we take regular backups
module "backups" {
  source = "./../../tf_backups"
  # Wait until the provisioning step is done before setting up backups
  depends_on = [ null_resource.provisioning ]
  ip_address = module.standard_vm.ip_address
  ssh_privkey = local.ssh_privkey
}
