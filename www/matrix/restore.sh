#!/bin/sh -e

echo "Restoring TLS certs"
if [ -f $HOME/*-letsencrypt.tar.xz ]; then
	mkdir -p /etc/letsencrypt  # Allows restoring certs before installing certbot
	cd /etc/letsencrypt
	tar Jxf $HOME/*-letsencrypt.tar.xz
	rm $HOME/*-letsencrypt.tar.xz
fi

if [ -f $HOME/*-synapse.pgdump ]; then
	echo "Moving postgresl backup to the expected location"
	mv $HOME/*-synapse.pgdump $HOME/synapse.pgdump
fi

if [ -f $HOME/*-homeserver.signing.key ]; then
	echo "Moving server signing key to the expected location"
	mv $HOME/*-homeserver.signing.key $HOME/homeserver.signing.key
fi

if [ -f $HOME/*-homeserver.yaml ]; then
	echo "Moving server configuration to the expected location"
	mv $HOME/*-homeserver.yaml $HOME/homeserver.yaml
fi

if [ -f $HOME/*-media.tar.xz ]; then
	echo "Moving media backups to the expected location"
	mv $HOME/*-media.tar.xz $HOME/media.tar.xz
fi
