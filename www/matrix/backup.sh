#!/bin/sh -e
DESTINATION_HOST="backup@backup.`hostname -d`"
DESTINATION_PATH="/var/backups/`hostname -f`"
TIMESTAMP=`date +%Y.%m.%d`

# Make sure the destination directory exists
ssh $DESTINATION_HOST "mkdir -p $DESTINATION_PATH"

# Back up the normal stuff
/root/resources/backup_host_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_ssh_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_known_hosts.sh $DESTINATION_HOST $DESTINATION_PATH $TIMESTAMP

# Backup things specific to this machine
cd /etc/letsencrypt
tar -Jc accounts live archive renewal | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/$TIMESTAMP-letsencrypt.tar.xz"
sudo -u postgres pg_dump synapse | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/$TIMESTAMP-synapse.pgdump"
cat /etc/matrix-synapse/homeserver.signing.key | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/$TIMESTAMP-homeserver.signing.key"
cat /etc/matrix-synapse/homeserver.yaml | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/$TIMESTAMP-homeserver.yaml"
cd /var/lib/matrix-synapse/media
tar -Jc . | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/$TIMESTAMP-media.tar.xz"
