#!/bin/sh -e
# Set the user account and systemd script to run the service
user="matrix"

useradd --create-home --shell /bin/bash $user

export DEBIAN_FRONTEND=noninteractive
apt install -yq git golang postgresql nginx python3-certbot-nginx

# Dendrite
su -c "cd; git clone https://github.com/matrix-org/dendrite" $user
cd /home/$user/dendrite
su -c "./build.sh" $user

# Kafka
# Skipping installing Kafka (it's optional in Monolith mode)

# Database configuration
# We use a random password for every installation and replace slashes with
# underscores so it doesn't cause problems when embedding the password into
# a URI (for the database)
pw=`dd if=/dev/urandom bs=16 count=1 2>/dev/null | base64 | sed 's|/|_|g'`
echo "create user dendrite password '$pw'" | sudo -u postgres psql
sudo -u postgres createdb -O dendrite dendrite

if [ -f $HOME/dendrite.pgdump ]; then
	echo "Restoring SQL database from backup file"
	sudo -u postgres psql dendrite < $HOME/dendrite.pgdump
fi

# Dendrite configuration
if [ -f $HOME/matrix_key.pem ]; then
	echo "Restoring matrix_key from backups"
	mv $HOME/matrix_key.pem .
	chown $user matrix_key.pem
else
	su -c "./bin/generate-keys --private-key matrix_key.pem" $user
fi
su -c "cp dendrite-config.yaml dendrite.yaml" $user
# Only replace the first occurence of server_name
su -c "sed -i \"0,/server_name: .*/{s/server_name: .*/server_name: `hostname -f`/}\" dendrite.yaml" $user
su -c "sed -i \"s|connection_string: .*|connection_string: postgres://dendrite:$pw@localhost/dendrite?sslmode=disable|\" dendrite.yaml" $user
su -c "sed -i \"s/use_naffka: .*/use_naffka: true/g\" dendrite.yaml" $user
su -c "sed -i \"0,/private_key: .*/{s|private_key: .*|private_key: /home/$user/dendrite/matrix_key.pem|}\" dendrite.yaml" $user
mkdir /var/log/dendrite
chown $user /var/log/dendrite

# Get TLS certs (if we didn't restore them from backup) and set up cron job to
# pull them regularly
chmod +x $HOME/set_up_letsencrypt.sh
$HOME/set_up_letsencrypt.sh --nginx -d `hostname -f` --email "admin@`hostname -d`"
rm $HOME/set_up_letsencrypt.sh
# Make sure the matrix user can get to the certificates
chown -R matrix /etc/letsencrypt/live
chown -R matrix /etc/letsencrypt/archive

# Set the systemd script to run the service
mv $HOME/matrix.service /lib/systemd/system/
systemctl enable matrix.service
systemctl start matrix.service

# The TLS entry for nginx proxy /_matrix to the dendrite server
sed -i 's|location / {|location /_matrix {|g' /etc/nginx/sites-available/default
# Security headers
sed -i '/^[^#].*location \/_matrix {/i add_header X-Frame-Options SAMEORIGIN;' /etc/nginx/sites-available/default
sed -i '/^[^#].*location \/_matrix {/i add_header X-Content-Type-Options nosniff;' /etc/nginx/sites-available/default
sed -i '/^[^#].*location \/_matrix {/i add_header X-XSS-Protection "1; mode=block";' /etc/nginx/sites-available/default
sed -i "/^[^#].*location \/_matrix {/i add_header Content-Security-Policy \"frame-ancestors 'none'\";" /etc/nginx/sites-available/default
sed -i "/^[^#].*location \/_matrix {/i add_header Access-Control-Allow-Origin \"*\";" /etc/nginx/sites-available/default
sed -i "/^[^#].*location \/_matrix {/i add_header Access-Control-Allow-Methods \"GET, POST, PUT, DELETE, OPTIONS\";" /etc/nginx/sites-available/default
sed -i "/^[^#].*location \/_matrix {/i add_header Access-Control-Allow-Headers \"Origin, X-Requested-With, Content-Type, Accept, Authorization\";" /etc/nginx/sites-available/default
# Proxy settings
sed -i '/^[^#].*location \/_matrix {/i proxy_set_header X-Forwarded-For $remote_addr;' /etc/nginx/sites-available/default
sed -i '/^[^#].*location \/_matrix {/i proxy_set_header X-Forwarded-Proto $scheme;' /etc/nginx/sites-available/default
sed -i 's|try_files.*|proxy_pass https://localhost:8448;|g' /etc/nginx/sites-available/default
# Allow people to upload images/videos over 1M (default)
sed -i '/^[^#].*location \/_matrix {/i client_max_body_size 50M;' /etc/nginx/sites-available/default

# Finally, if we have a config file to restore, we use that to overwrite
# everything we just set up
if [ -f $HOME/default ]; then
	mv $HOME/default /etc/nginx/sites-available/default
fi

# Restart nginx to pick up the updated config
systemctl restart nginx
