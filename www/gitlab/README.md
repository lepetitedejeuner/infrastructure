# GitLab server
GitLab is a web interface similar to GitHub.com, but GitLab can be self-hosted.
GitLab provides issue tracking, the ability to easily branch and merge via a
web interface, project wikis, and a lot of CI/CD options.  The basic features
are available for free, while features that are generally only of interest to
larger companies involve purching licenses for each user.

There are [two editions](https://about.gitlab.com/install/ce-or-ee/) of GitLab:
Community and Enterprise.  This script sets up the enterprise edition.  It does
include some code that is not MIT Expat license.  Nothing it installs costs any
money.`

# Customization
To deploy your own GitLab server, just:
- [Do the normal things](../../README.md#Customization)

# Testing
Scripts tested with:
- Ubuntu 18.04
- Ubuntu 20.04

# Troubleshooting
Useful commands for being a GitLab sysadmin:

* `gitlab-ctl tail`
* `gitlab-ctl status`
* `gitlab-ctl restart`

# Opportunities for improvment
TLS is currently not set up by these deploy scripts
