#!/bin/sh -e
# This just demonstrates how to upgrade it. Upgrades are not done automatically
# on account of not wanting to introduce unexpected downtime.
sudo apt-get update
sudo apt-get install gitlab-ee
