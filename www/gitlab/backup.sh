#!/bin/bash
# Back up to a remote machine
# install_backups.sh usually installs this script in /root
# Usually invoked by root's crontab
#### PREREQUISITES
# - All vagrant provisioning is complete
# - Root's ssh key is in .ssh/authorized_keys on the backup server

DESTINATION_HOST=vagrant@backup.example.com
DESTINATION_PATH=/mnt/backup/`hostname -f`

# The backup_gitlab script will also backup host keys, ssh keys, and known hosts
/root/resources/backup_gitlab.sh $DESTINATION_HOST $DESTINATION_PATH
