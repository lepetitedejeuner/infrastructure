#!/bin/sh -e
# This script is to be run from the server.  It will back everything
# up and then copy it to the appropriate backup location.
DESTINATION_HOST="vagrant@backup.example.com"
DESTINATION_PATH="/mnt/backup/`hostname -f`"
TIMESTAMP=`date +%Y.%m.%d`

echo "Backups will be copied to $DESTINATION_HOST:$DESTINATION_PATH"
/root/resources/backup_host_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_ssh_keys.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH
/root/resources/backup_known_hosts.sh $TIMESTAMP $DESTINATION_HOST $DESTINATION_PATH

scp /etc/influxdb/influxdb.conf $DESTINATION_HOST:$DESTINATION_PATH/$TIMESTAMP-influxdb.conf
# According to the docs, omitting -database <db_name> will back up all
# databases.  Empirical testing confirms that this is, in fact the case.
# Source: https://docs.influxdata.com/influxdb/v1.7/administration/backup_and_restore/
influxd backup -portable ./${TIMESTAMP}-influxdb
tar c ${TIMESTAMP}-influxdb/ | ssh $DESTINATION_HOST "cat - > $DESTINATION_PATH/${TIMESTAMP}-influxdb.tar"
rm -r ${TIMESTAMP}-influxdb/
