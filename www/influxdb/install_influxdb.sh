#!/bin/sh -e
# This script installs influxdb
# Assumes this will be run as root and that ~/influxdb_password exists
# Usage: $0 IP [FQDN]
IP="$1"
FQDN="$2"

echo "deb https://repos.influxdata.com/ubuntu bionic stable" > /etc/apt/sources.list.d/influxdb.list
curl -sL https://repos.influxdata.com/influxdb.key | apt-key add -
apt-get update
apt-get install -y influxdb

# Put our configuration in the right place
cd $HOME
if [ -f influxdb.conf ]; then
	mv influxdb.conf /etc/influxdb/influxdb.conf
fi
# Make sure influxdb is listening on the correct interface
sed -i "s/bind-address = \"[^:]*/bind-address = \"$IP/g" /etc/influxdb/influxdb.conf
if [ ! -z "$FQDN" ]; then
	# Make sure that if /etc/hosts points anywhere, it's pointing to that same IP
	sed -i -e "s/.*$FQDN/$IP  $FQDN/g"  /etc/hosts
fi
# Disable the "feature" that sends your usage data to a 3rd party
sed -i 's/# *reporting-disabled = .*/reporting-disabled = true/' /etc/influxdb/influxdb.conf
# Enable it (start on boot) and start the service
echo "Starting influxdb"
systemctl enable influxdb
systemctl start influxdb
sleep 5  # Give it a few seconds to start up

# reate admin user
ap=`dd if=/dev/random bs=16 count=1 2> /dev/null | base64`
echo "$ap" > $HOME/influxdb_admin_password  # Save it for later
echo "CREATE USER admin WITH PASSWORD '$ap' WITH ALL PRIVILEGES" | influx -host $IP -port 8086

# Set up a user to store server health information (via telegraf)
p=`cat $HOME/influxdb_password`
echo "CREATE DATABASE telegraf" | influx -host $IP -port 8086
echo "CREATE USER telegraf WITH PASSWORD '$p'" | influx -host $IP -port 8086
echo "GRANT ALL ON telegraf TO telegraf" | influx -host $IP -port 8086
echo "CREATE RETENTION POLICY thirty_days ON telegraf DURATION 30d REPLICATION 1 DEFAULT" | influx -host $IP -port 8086

# Enable authentication
sed -i 's/# *auth-enabled =.*/auth-enabled = true/' /etc/influxdb/influxdb.conf
# Note: Authentication is not actually enforced unless there is an admin user
# https://docs.influxdata.com/influxdb/v1.8/administration/authentication_and_authorization/#authorization
systemctl restart influxdb
