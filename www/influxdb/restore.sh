#!/bin/sh -e
# This restores influxdb data from a backup
# Assumes it will be run as root, assumes TLS certs have been restored
# Requires
#   Timestamp of the backup be given as an argument
#   The backups are located in ~root/ and named ${TIMESTAMP}-*
cd $HOME
TIMESTAMP=`ls *-influxdb.tar | tail -n 1 | sed -e 's/-influxdb.tar//g'`
IP="$1"

if [ -z "$TIMESTAMP" ]; then
	echo "No database to restore"
	exit 0
fi

echo "Restoring configuration file..."
mv ${TIMESTAMP}-influxdb.conf /etc/influxdb/influxdb.conf

echo "Restoring database..."
tar xf ${TIMESTAMP}-influxdb.tar
# The documentation says omitting -db <db_name> will restore all databases. The
# docs lie.  Imperical testing confirms this only restore the telegraf database
# and not the other databases.  So, we'll explicitly restore each database to
# make sure things are restored properly!
# Source: https://docs.influxdata.com/influxdb/v1.7/administration/backup_and_restore/

# If you have other databases in influxdb, you'll need to restore each one of
# them separately (unless the influxdb code is updated to match the docs)
#influxd restore -db otherdb1 -portable *-influxdb
#influxd restore -db otherdb2 -portable *-influxdb


# We can't overwrite existing databases using the restore command, so we need
# to drop the existing "telegraf" database, but we need to stop telegraf before
# we can do that.
echo "Dropping the telegraf database so the old one can be restored"
echo "drop database telegraf" | influx -host 127.0.0.1 -port 8086 \
    -username admin -password `cat influxdb_admin_password`

echo "Restoring telegraf database..."
influxd restore -db telegraf -portable *-influxdb

# Dropping the database also removed the user's permissions to that database
# and permissions are not automatically restored.  As such, we need to re-grant
# those permissions now.
echo 'GRANT ALL ON "telegraf" TO "telegraf"' | influx -host 127.0.0.1 -port 8086 \
    -username admin -password `cat influxdb_admin_password`

# Clean up the decompressed directory of files
rm -R *-influxdb
rm ${TIMESTAMP}-influxdb.tar  # and the tar file

systemctl restart influxdb
