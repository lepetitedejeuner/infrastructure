# Influxdb server
This is a time-series database.  It stores values that are going to be recorded
on a periodic basis.  This could be the temperatue in the server room, the
humidity of a wine cellar, or (in our case) the health metrics of a server such
as CPU load averages, disk and memory usage and so forth.

This server will just hold that data. Tools like [Grafana](../grafana) can then
be used to view this data.

# Passwords
Unlike the [nextcloud](../nextcloud) demo, this example follows best practices
and does not store the password for the database used in the git repo. Instead,
you must create a file with this password in it and deploy that.  Be sure to
have a copy in your password manager!

This demo will randomly generate a password for the admin user and record it in
a text file that is only readable by root in root's home directory (/root).  It
also requires having a `influxdb_password` file in this directory that contains
the non-admin database user's password.  This same file will need to be present
in any deployment that will write to the influxdb "telegraf" database.

It should be possible to have one user that can only write (which servers would
use when recording their health metrics) and another one for reading (which
would be used by the grafana server), but this example does not do this.

# Customization
To try this one out for your domain, you'll want to:
- [Do the normal things](../../README.md#Customization)
- Put a password for the database user in influxdb_password

# Testing
Scripts tested with:
- Ubuntu 18.04

# Troubleshooting
To connect to the database using the command line influx clinet, try this:
```bash
influx -host 192.168.0.204 -port 8086 -database telegraf -username telegraf -password whatever
```

To test running queries, try something like this:
```
SELECT * FROM processes LIMIT 10;
```

If you get an error like `ERR: authorization failed`, or `ERR: error authorizing query: telegraf not authorized to execute statement 'SELECT * FROM processes LIMIT 10', requires READ on telegraf` you will want to connect as an admin and
take a look at the users.  When you do so, it should show something like this:

```
root@influxdb:~# influx -host 127.0.0.1 -port 8086 -username admin -password `cat influxdb_admin_password`
Connected to http://127.0.0.1:8086 version 1.8.2
InfluxDB shell version: 1.8.2
> show users
user     admin
----     -----
admin    true
telegraf false
```

You can see the permissions for a user with `SHOW GRANTS for telegraf`.  The
output should show that telegraf has al privileges to that table

```
> SHOW GRANTS for telegraf
database privilege
-------- ---------
telegraf ALL PRIVILEGES
```

