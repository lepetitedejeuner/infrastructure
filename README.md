# Infrastructure
This repo exists just to serve as examples of using Infrastructure as Code.  It
is intended to demonstrate the concepts in an easy-to-use environment such as a
desktop/laptop.  More elaborate automated deployments are possible, of course,
but for those who are new to the topic, this should get you going.

# Customization
There are a number of things you'll normally want to do before running
`vagrant up` on any of these VMs.

- Change the networking settings (ip, gateway, and dns) in Vagrantfile
- Change hostname in Vagrantfile
- If you want an SSH key added to the vagrant user on this VM, put your public
  key in ../../resources as your_name.pub and change "alice" in the Vagrantfile
  to be "your_name"
- Update backup.sh to copy files to a real backup server (see
  [backup](ssh/backup)) for an example
- Put root's public SSH key in the authorized_keys file on the backup server
- After booting the VM, su over to root, run ~/backup.sh so you can verify and
  accept the SSH host key of the backup server
- (Optional) Put a password in `~root/backup_password`  This will encrypt the
  backups before sending them to the backup server.  This is a good idea if you
  are backing up to a server you don't control, such as one running on a cloud
  provider.  If this is done when backing up files, the encrypted backup files
  can be put in this directory in order to restore the data on a redeploy.
